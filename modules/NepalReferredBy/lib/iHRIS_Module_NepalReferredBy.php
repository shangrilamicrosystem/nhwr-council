<?php
class iHRIS_Module_NepalReferredBy extends I2CE_Module {
    public static function getMethods() {
        return array(
            'iHRIS_PageView->action_nepal_referred_by' => 'action_nepal_referred_by',
            );
    }
 
 
    public function action_nepal_referred_by($obj) {
        if (!$obj instanceof iHRIS_PageView) {
            return;
        }
        return $obj->addChildForms('nepal_referred_by', 'siteContent');
    }
}
?>