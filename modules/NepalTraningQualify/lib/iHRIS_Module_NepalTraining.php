<?php
class iHRIS_Module_NepalTraining extends I2CE_Module {
    public static function getMethods() {
        return array(
            'iHRIS_PageView->action_nepal_training' => 'action_nepal_training',
            );
    }
 
 
    public function action_nepal_training($obj) {
        if (!$obj instanceof iHRIS_PageView) {
            return;
        }
        return $obj->addChildForms('nepal_training', 'siteContent');
    }
}
?>