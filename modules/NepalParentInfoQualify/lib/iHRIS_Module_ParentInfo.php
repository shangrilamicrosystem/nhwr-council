<?php
class iHRIS_Module_ParentInfo extends I2CE_Module {
    public static function getMethods() {
        return array(
            'iHRIS_PageView->action_person_parent_info' => 'action_person_parent_info',
            );
    }
 
 
    public function action_person_parent_info($obj) {
        if (!$obj instanceof iHRIS_PageView) {
            return;
        }
        return $obj->addChildForms('person_parent_info', 'siteContent');
    }
}
?>