<?php
class iHRIS_Module_EmploymentSummary extends I2CE_Module {
    public static function getMethods() {
        return array(
            'iHRIS_PageView->action_person_employment_summary' => 'action_person_employment_summary',
            );
    }
 
 
    public function action_person_employment_summary($obj) {
        if (!$obj instanceof iHRIS_PageView) {
            return;
        }
        return $obj->addChildForms('person_employment_summary', 'siteContent');
    }
}
?>