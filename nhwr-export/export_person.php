<?php
error_reporting(E_ALL); ini_set('display_errors', 1);
require_once("../nhwr-import/import_base.php");
require_once "export_base.php";
require_once "export_person_data.php";

$sql = <<<SQL
SELECT 
	p.id,p.uuid, p.firstname, p.middle_name, p.surname, p.maiden_name, p.othername, p.othername_2, p.othername_3,
	c.name citizenship, c2.name citizenship_secondary,
	c_c.name residence_country, c_r.name residence_region, c_d.name residence_district, c_v.name residence_vdc_mun,
	p.ward_number, p.street_name, p.house_number, p.nationality, p.residence, p.nationality_secondary, p.temp_address, p.first_name_np, p.middle_name_np,p.last_name_np,p.temp_address_ward,p.temp_address_street,p.temp_address_house

FROM hippo_person p
	LEFT JOIN hippo_country c on p.nationality = c.id
	LEFT JOIN hippo_country c2 on p.nationality_secondary = c2.id
	LEFT JOIN hippo_county c_v on p.residence = c_v.id
	LEFT JOIN hippo_district c_d on c_v.district = c_d.id or p.residence = c_d.id
	LEFT JOIN hippo_region c_r on c_d.region = c_r.id or p.residence = c_r.id
	LEFT JOIN hippo_country c_c on c_r.country = c_c.id or p.residence = c_c.id
SQL;

$db = new DB();
$db->connect();
// echo "<pre>";
function append_header(&$header, $record){
	foreach ($record as $key=>$value) {
		array_push($header, $key);
	}
}

//executing main sql related to person
$result = $db->conn->query($sql);
if($result->num_rows > 0){
	$cnt = 0;
	// output data of each row
	$all_person = array();
    while($row = $result->fetch_assoc()) {
		//now adding row data related to the person (person detail & demographics)
		$person = array();
		$data = array($cnt+1);
		foreach ($row as $key=>$value) {
			$person[$key] = $value;
		}
		// print_r($person); exit;
		// info class 
		$frm = new form_person_data($row["id"]);

		if($person['nationality']){
			$person['nationality'] = $frm->getName($person['nationality']);
		}

		if($person['residence']){
			$person['residence'] = $frm->getName( $person['residence'] );
		}
		if($person['temp_address']){
			$person['temp_address'] = $frm->getName( $person['temp_address'] );
		}
		if($person['nationality_secondary']){
			$person['nationality_secondary'] = $frm->getName( $person['nationality_secondary'] );
		}
		
			
		//parent info
		$person['person_parent_info'] = $frm->getParentInfo();

		//identification
		$person['person_id'] = $frm->getPersonIds();
		
		//demographic
		$person['demographic'] = $frm->getDemographic();

		//person_contact_personal
		$person['person_contact_personal'] = $frm->getPersonalContact();
		//person_contact_emergency
		$person['person_contact_emergency'] = $frm->getEmergencyContact();
		//nepal_training
		$person['nepal_training'] = $frm->getNepalTraining();
		
		// nepal_refered_by
		$person['nepal_refered_by'] = $frm->getNepalReferredBy();

		// print_r($person);
		$all_person[] = $person;
	}
	

	$all_data = json_encode($all_person, JSON_PRETTY_PRINT);
	// print_r($all_data); exit;
	file_put_contents(dirname(__FILE__)."/data/person.json", $all_data);
	
	echo "complete!!\n";
}
else{
	echo "No records found.";
}
$db->close();
