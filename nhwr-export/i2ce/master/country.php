<?php
require_once dirname(__FILE__)."/../form_base.php";

class form_master_country extends form_base{
	function __construct()
	{
		
	}

	function prepareSql(){
		$sql = "
        SELECT id, name, alpha_two, code, csd_uuid FROM `hippo_country`
        ORDER BY `hippo_country`.`code`  DESC";

		return $sql;
	}
}