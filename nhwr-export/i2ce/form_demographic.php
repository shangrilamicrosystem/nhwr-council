<?php
require_once "form_base.php";

class form_demographic extends form_base{
	function __construct()
	{
		$this->form = "demographic";
		$this->transpose = false;
	}

	function prepareSql($parent, $param){
		if(strlen($parent) == 0)
			return null;
		$sql = "
SELECT 
	d.birth_date, g.name, ms.name, c.name citizenship_at_birth,
	c_c.name birth_country, c_r.name birth_region, c_d.name birth_district, c_v.name birth_vdc_mun,
	d.birth_location_other, d.dependents
FROM (SELECT * FROM hippo_demographic where parent = '$parent') d
	LEFT JOIN hippo_gender g on d.gender = g.id
	LEFT JOIN hippo_marital_status ms on d.marital_status = g.id
	LEFT JOIN hippo_country c on d.nationality_at_birth = c.id
	LEFT JOIN hippo_county c_v on d.birth_location = c_v.id
	LEFT JOIN hippo_district c_d on c_v.district = c_d.id or d.birth_location = c_d.id
	LEFT JOIN hippo_region c_r on c_d.region = c_r.id or d.birth_location = c_r.id
	LEFT JOIN hippo_country c_c on c_r.country = c_c.id or d.birth_location = c_c.id";
		return $sql;
	}

}