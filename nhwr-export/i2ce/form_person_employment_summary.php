<?php
require_once "form_base.php";

class form_person_employment_summary extends form_base{
	function __construct()
	{
		$this->form = "person_employment_summary";
		$this->parent_form = "person";
	}

	function prepareSql($parent, $param){
		if(strlen($parent) == 0)
			return null;
		$p_arr = explode("|", $parent);
		var_dump($p_arr);
		$parent_id = $p_arr[1];
		$sql = "
SELECT 
	fld.name, fld.type,
	CASE 
		WHEN fld.type = 'string' then 
			CASE WHEN cls.name is not null THEN cls.name 
			when es.name is not null then es.name else e.string_value end
		WHEN fld.type = 'integer' then e.integer_value
		WHEN fld.type = 'text' then e.text_value
		WHEN fld.type = 'date' then e.date_value
	end value
FROM form_field ff
	INNER JOIN form f on ff.form = f.id
	INNER JOIN field fld on ff.field = fld.id
	LEFT JOIN record r on f.id = r.form and parent_form = '$this->parent_form' AND r.parent_id = $parent_id
	LEFT JOIN last_entry e on ff.id = e.form_field and e.record = r.id 
	LEFT JOIN field_sequence fs on ff.id = fs.form_field
	LEFT JOIN hippo_classification cls on e.string_value = cls.id
	LEFT JOIN hippo_employment_status es on e.string_value = es.id
WHERE f.name = 'person_employment_summary'
ORDER BY fs.sequence";



return "SELECT 
	fld.name, fld.type,
	CASE 
		WHEN fld.type = 'integer' then e.integer_value
		WHEN fld.type = 'text' then e.text_value
		WHEN fld.type = 'date' then e.date_value
	end value
FROM form_field ff
	INNER JOIN form f on ff.form = f.id
	INNER JOIN field fld on ff.field = fld.id
	LEFT JOIN record r on f.id = r.form and parent_form = 'person' AND r.parent_id = 10367
	LEFT JOIN last_entry e on ff.id = e.form_field and e.record = r.id 
	LEFT JOIN field_sequence fs on ff.id = fs.form_field
WHERE f.name = 'person_employment_summary'
ORDER BY fs.sequence";
echo $sql; exit;
		return $sql;
	}

}