#!/usr/bin/php
<?php
/*
 * © Copyright 2007, 2008 IntraHealth International, Inc.
 *
 * This File is part of iHRIS
 *
 * iHRIS is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * The page wrangler
 *
 * This page loads the main HTML template for the home page of the site.
 * @package iHRIS
 * @subpackage DemoManage
 * @access public
 * @author Sovello Hildebrand sovellohpmgani@gmail.com
 * @copyright Copyright &copy; 2007, 2008-2013 IntraHealth International, Inc.
 * @version 4.6.0
 */
/*
php import_data.php ./data/person/person_2_4.json
php /var/lib/iHRIS/sites/nhwr-ihris/pages/import_data.php /var/lib/iHRIS/sites/nhwr-ihris/pages/data/person/person_2_4.json
php import_data.php /path/to/your/excel_sheet.csv
*/
require_once("./import_base.php");

class PersonalData_Import extends Processor{

	public function __construct($file) {
			parent::__construct($file);
	}

	//map headers from the spreadsheet
	//what you do here is change the values on the right to match what you have on the spreadsheet. comment out lines that are not in the spreadsheet
	//the values of the left are used by the script to refer to the spreadsheet columns on the right of this array.
	//the order of the columns in the spreadsheet doesn't matter

	protected function getExpectedHeaders(){
		return array(
			'uuid'		=> 'uuid',
			"firstname" => "firstname",
			"middle_name" => "middle_name",
			"surname" => "surname",
			"maiden_name" => "maiden_name",
			"othername" => "othername",
			"othername_2" => "othername_2",
			"othername_3" => "othername_3",
			"citizenship" => "citizenship",
			"citizenship_secondary" => "citizenship_secondary",
			"residence_country" => "residence_country",
			"residence_region" => "residence_region",
			"residence_district" => "residence_district",
			"residence_vdc_mun" => "residence_vdc_mun",
			"ward_number" => "ward_number",
			"street_name" => "street_name",
			"house_number" => "house_number",
			"nationality" => "nationality",
			"residence" => "residence",
			"nationality_secondary" => "nationality_secondary",
			"temp_address" => "temp_address",
			"first_name_np" => "first_name_np",
			"middle_name_np" => "middle_name_np",
			"last_name_np" => "last_name_np",
			"temp_address_ward" => "temp_address_ward",
			"temp_address_street" => "temp_address_street",
			"temp_address_house" => "temp_address_house",
		
			"person_id"	=> "person_id",
			"demographic" => "demographic",
			"person_contact_personal" => "person_contact_personal",
			"person_contact_emergency" => "person_contact_emergency",
			"person_parent_info" => "person_parent_info",
			"nepal_training" => "nepal_training",
			"nepal_refered_by" => 'nepal_refered_by'

		);
	}

	/**
	 * this function returns the id numbers 'id_number' and id type 'id_type' for that id number.
	 */

	public function getIdNumberArray(){
		//when in the spreadsheet you have multiple columns that refer to identification numbers then you have this array to handle just that
		//with the columns set above, here you come map the columns to their specific id_type.
		//so if you have say just one column with identification numbers then comment lines 80 to 87 of this file by typing /* on line 79
		//and */ on line 89
		//change the the value in id_type to be e.g. National ID to match the type of identification for this column
		return $id_numbers = array(
			0 => array(
					'id_number' => $this->mapped_data['id_num'],
					'id_type' => 'Payroll Number'
				),
			/*
			1 => array(
					'id_number' => $this->mapped_data['id_num1'],
					'id_type' => 'Salary Number'
				),
			2 => array(
				 'id_number' => $this->mapped_data['id_num'],
					'id_type' => 'Payroll Number'
			*/

		);
	}

	//this part checks to see if the nationality/country column for a record is empty it defaults to Tanzania.
	//change this to your country. otherwise it takes the value of the country in that column
	public function getNationality(){
		return empty($this->mapped_data['nationality']) ? 'Nepal' : $this->mapped_data['nationality'];
	}

	//in this part comment out if you are not adding any data for that specific item.
	//for example if there is no nextofkin data in the spreadsheet,
	//comment out line 108 by preceding it with double-slasses as in this line
	//remember to also comment out all lines in the getExpectedHeaders() function lines 46-63
	protected function _processRow(){
		$personId = $this->findPerson($this->mapped_data);
		//$personId = $this->findPersonByReg($reg_data["council"], $reg_data["registration_number"], $reg_data["registration_date"]);
		I2CE::raiseMessage("personId = $personId");
		if(strlen($personId)==0)
			return;
		//start updating the child data
		$this->updatePersonIds($personId, $this->mapped_data['person_id']);
		$this->updateDemographic($personId, $this->mapped_data['demographic']);
		$this->updateParentInfo($personId, $this->mapped_data['person_parent_info']);
		$this->updatePersonalContact($personId, $this->mapped_data["person_contact_personal"]);
		$this->updateEmergencyContact($personId, $this->mapped_data["person_contact_emergency"]);
		$this->updatePersonTraining($personId, $this->mapped_data["nepal_training"]);
	}

	protected function updatePersonIds($personId, $Ids = array() ){
		if( ($personObj = $this->ff->createContainer($personId)) instanceof iHRIS_Person ){
			foreach( $Ids as $index=>$data){
				$formObj = $this->ff->createContainer('person_id');
				$formObj->getField('id_num')->setValue(trim($data['id_num']));
				$formObj->getField('id_type')->setValue(array('id_type', $this->idTypeExists($data['id_type']) ) );
				$formObj->getField('country')->setValue(array('country', $this->countryExists($data['country']) ) );
				$formObj->getField('place')->setValue(trim($data['place']));
				$formObj->setParent($personId);
				$this->save($formObj);
			}
		}
	}

	protected function updatePersonTraining($personId, $Ids = array() ){
		if( ($personObj = $this->ff->createContainer($personId)) instanceof iHRIS_Person ){
			foreach( $Ids as $index=>$data){
				$formObj = $this->ff->createContainer('nepal_training');
				$formObj->getField('country')->setValue(array('country', $this->countryExists($data['country']) ) );
				$formObj->getField('institution')->setValue(trim($data['institution']));
				$formObj->getField('location_other')->setValue(trim($data['location_other']));
				$formObj->getField('started_date')->setValue(I2CE_Date::fromDB(trim($data['started_date'])));
				$formObj->getField('completed_date')->setValue(I2CE_Date::fromDB(trim($data['completed_date'])));
				$formObj->getField('minor')->setValue(trim($data['minor']));
				
				$formObj->setParent($personId);
				$this->save($formObj);
			}
		}
	}

	protected function updateParentInfo($personId, $parentDetail){
		if($personId)
		$where = array(
			'operator' => 'FIELD_LIMIT',
			'field' => 'parent',
			'style' => 'lowerequals',
			'data' => array(
					'value' => $personId
				)
			);

		$parentId = I2CE_FormStorage::search('person_parent_info', false, $where );
		if( count($parentId) == 0){
			$formObj = $this->ff->createContainer('person_parent_info');
		}
		else
			$formObj = $this->ff->createContainer('person_parent_info|'.current($parentId));
		$formObj->getField('father_surname')->setValue(trim($parentDetail['father_surname']));
		$formObj->getField('father_firstname')->setValue(trim($parentDetail['father_firstname']));
		$formObj->getField('father_middlename')->setValue(trim($parentDetail['father_middlename']));
		
		$formObj->getField('mother_surname')->setValue(trim($parentDetail['mother_surname']));
		$formObj->getField('mother_firstname')->setValue(trim($parentDetail['mother_firstname']));
		$formObj->getField('mother_middlename')->setValue(trim($parentDetail['mother_middlename']));
		
		
		$formObj->getField('spouse_firstname')->setValue(trim($parentDetail['spouse_firstname']));
		$formObj->getField('spouse_middlename')->setValue(trim($parentDetail['spouse_middlename']));
		$formObj->getField('spouse_surname')->setValue(trim($parentDetail['spouse_surname']));

		$formObj->getField('grand_father_firstname')->setValue(trim($parentDetail['grand_father_firstname']));
		$formObj->getField('grand_father_middlename')->setValue(trim($parentDetail['grand_father_middlename']));
		$formObj->getField('grand_father_surname')->setValue(trim($parentDetail['grand_father_surname']));

		$formObj->setParent($personId);
		$this->save($formObj);
	}

	protected function updatePersonalContact($personId, $contact){
		if(strlen($personId)==0)
			return;
		if(trim($contact['telephone']) == 0 && trim($contact['mobile_phone']) && trim($contact['email']))
			return;
		$where = array(
			'operator' => 'FIELD_LIMIT',
			'field' => 'parent',
			'style' => 'lowerequals',
			'data' => array(
					'value' => $personId
				)
			);

		$parentId = I2CE_FormStorage::search('person_contact_personal', false, $where );
		if( count($parentId) == 0){
			$formObj = $this->ff->createContainer('person_contact_personal');
		}
		else
			$formObj = $this->ff->createContainer('person_contact_personal|'.current($parentId));
		$formObj->getField('telephone')->setValue(trim($contact['telephone']));
		$formObj->getField('mobile_phone')->setValue(trim($contact['mobile_phone']));
		// $formObj->getField('alt_telephone')->setValue(trim($contact['alt_telephone']));
		$formObj->getField('email')->setValue(trim($contact['email']));
		$formObj->setParent($personId);
		$this->save($formObj);
	}

	protected function updateEmergencyContact($personId, $contact){
		if($personId)
		$where = array(
			'operator' => 'FIELD_LIMIT',
			'field' => 'parent',
			'style' => 'lowerequals',
			'data' => array(
					'value' => $personId
				)
			);

		$parentId = I2CE_FormStorage::search('person_contact_emergency', false, $where );
		if( count($parentId) == 0){
			$formObj = $this->ff->createContainer('person_contact_emergency');
		}
		else
			$formObj = $this->ff->createContainer('person_contact_emergency|'.current($parentId));
		$formObj->getField('surname')->setValue(trim($contact['surname']));
		$formObj->getField('middlename')->setValue(trim($contact['middlename']));
		$formObj->getField('firstname')->setValue(trim($contact['firstname']));
		$formObj->getField('telephone')->setValue(trim($contact['telephone']));
		$formObj->getField('mobile_phone')->setValue(trim($contact['mobile_phone']));
		$formObj->getField('alt_telephone')->setValue(trim($contact['alt_telephone']));
		$formObj->getField('email')->setValue(trim($contact['email']));
		$formObj->getField('mailing_address')->setValue(trim($contact['mailing_address']));
		$formObj->setParent($personId);
		$this->save($formObj);
	}
	/****************************************************************************
	 *                                                                          *
	 *   DON'T EDIT BEYOND THIS POINT UNLESS YOU KNOW WHAT YOU WANT TO ACHIEVE  *
	 *                                                                          *
	 ****************************************************************************/
	protected function createPersonId($personId, $id_nums = array() ){
		if( ($personObj = $this->ff->createContainer($personId)) instanceof iHRIS_Person ){
			foreach( $id_nums as $index=>$data){
				$formObj = $this->ff->createContainer('person_id');
				$formObj->getField('id_num')->setValue(trim($data['id_number']));
				$formObj->getField('id_type')->setValue(array('id_type', $this->idTypeExists($data['id_type']) ) );
				$formObj->setParent($personId);
				$this->save($formObj);
			}
		}
	}

	protected function updateDemographic($personId, $demo){
		if(strlen($personId)==0)
			return;
		$where = array(
			'operator' => 'FIELD_LIMIT',
			'field' => 'parent',
			'style' => 'lowerequals',
			'data' => array(
					'value' => $personId
				)
			);

		$demoId = I2CE_FormStorage::search('demographic', false, $where );
		// echo current($demoId); exit;
		I2CE::raiseMessage("personId = $personId, demoId count ".count($demoId));
		if( count($demoId) == 0){
			$formObj = $this->ff->createContainer('demographic');
		}
		else
			$formObj = $this->ff->createContainer("demographic|".current($demoId));

		$formObj->getField('birth_date')->setValue(I2CE_Date::fromDB(trim($demo['birth_date'])));
		$formObj->getField('gender')->setValue( array('gender', $this->getGender($demo['gender'])));
		$formObj->getField('marital_status')->setValue( array('marital_status', $this->maritalStatusExists($demo['marital_status'])) );
		$formObj->getField('dependents')->setValue($demo['dependents']);
		$formObj->setParent($personId);
		$this->save($formObj);
	}

	public function countryExists($name){
		return $this->checkNameExists('country', $name);
	}
	public function countyExists($name){
		return $this->checkNameExists('county', $name);
	}
	
	public function regionExists($name){
		return $this->checkNameExists('region', $name);
	}
	public function currencyExists($name){
		return $this->checkNameExists('currency', $name);
	}
	
	public function idTypeExists($name){
		return $this->checkNameExists('id_type', $name);
	}
	public function jobExists($title){
		return $this->checkTitleExists('job', $title);
	}
	public function facilityExists($name){
		return $this->checkNameExists('facility', $name);
	}
	public function councilExists($name){
		return $this->checkNameExists('council', $name);
	}
	public function districtExists($name){
		return $this->checkNameExists('district', $name);
	}

	public function maritalStatusExists($name){
		return $this->checkNameExists('marital_status', $name);
	}

	public function getGender( $gender ){

		$g = strtolower(trim($gender));
		if( ($g == 'f') || ($g == 'female') || (substr($gender,0,1) == 'f') ){
			return 'F';
		}
		if( ($g == 'm') || ($g == 'male') || (substr($gender,0,1) == 'm') ){
			return 'M';
		}
	}

	public function findPersonByNames($surname, $firstname, $middle_name, $citizenship, $createRecord = true){
		//search person by names
		$wherePerson = array(
			'operator'=>'AND',
			'operand'=>array(
				0=>array(
						'operator'=>'FIELD_LIMIT',
						'field'=>'surname',
						'style'=>'lowerequals',
						'data'=>array(
								'value'=>trim($surname)
								)
						),
				1=>array(
						'operator'=>'FIELD_LIMIT',
						'field'=>'firstname',
						'style'=>'lowerequals',
						'data'=>array(
								'value'=>trim($firstname)
								)
						),
				2=>array(
						'operator'=>'FIELD_LIMIT',
						'field'=>'middle_name',
						'style'=>'lowerequals',
						'data'=>array(
								'value'=>trim($middle_name)
								)
						),
				// 3=>array(
				// 		'operator'=>'FIELD_LIMIT',
				// 		'field'=>'country',
				// 		'style'=>'lowerequals',
				// 		'data'=>array(
				// 				'value'=>array('country', $this->countryExists($citizenship))
				// 				)
				// 		),
				)
			);

		$person_ids = I2CE_FormStorage::search('person', false, $wherePerson);
		if( count($person_ids) > 1 || $createRecord == false){
			//$this->addBadRecord("This person already exists in duplicates");
			// return false;
			$arr = array();
			foreach ($person_ids as $key => $value) {
				$arr[] = 'person|'.$value;
			}
			return $arr;
			// return $person_ids;
		}
		elseif(count($person_ids) == 1){
			return 'person|'.$person_ids[0];
		}
		elseif( (count($person_ids) == 0) && createRecord ){
			//we need to create this person
			$personObj = $this->ff->createContainer('person');
			$personObj->firstname = trim($firstname);
			$personObj->othername = trim($othername);
			$personObj->surname = trim($surname);
			$personObj->nationality = array('country', $this->countryExists($country) );
			$personId = $this->save($personObj);
			return 'person|'.$personId;
		}
	}

	protected function createPerson($data, $person_id = false){
		$dt = $data;
		if( !$person_id )
			$personObj = $this->ff->createContainer('person');
		else
			$personObj = $this->ff->createContainer($person_id);
			
		$personObj->firstname = trim($dt["firstname"]);
		$personObj->middle_name = trim($dt["middle_name"]);
		$personObj->surname = trim($dt["surname"]);
		$personObj->maiden_name = trim($dt["maiden_name"]);
		$personObj->othername = trim($dt["othername"]);
		$personObj->othername_2 = trim($dt["othername_2"]);
		$personObj->othername_3 = trim($dt["othername_3"]);
		$personObj->citizenship = array('country', $this->countryExists($dt['citizenship']) );
		$personObj->citizenship_secondary = array('country', $this->countryExists($dt['citizenship_secondary']) );
		$personObj->residence_country = array('country', $this->countryExists($dt['residence_country']) );
		$personObj->residence_region = array('region', $this->regionExists($dt['residence_region']) );
		$personObj->residence_district = array('district', $this->districtExists ($dt['residence_district']) );
		$personObj->residence_vdc_mun = trim($dt["residence_vdc_mun"]);
		$personObj->ward_number = trim($dt["ward_number"]);
		$personObj->street_name = trim($dt["street_name"]);
		$personObj->house_number = trim($dt["house_number"]);
		$personObj->nationality = array('country', $this->countryExists($dt['nationality']) );
		$personObj->residence = array('county', trim($this->countyExists($dt['residence'])));
		$personObj->nationality_secondary = array('country', $this->countryExists($dt['nationality_secondary']) );
		$personObj->temp_address = array('county', $this->countyExists($dt['temp_address']) );
	
		$personObj->first_name_np = trim($dt["first_name_np"]);
		$personObj->middle_name_np = trim($dt["middle_name_np"]);
		$personObj->last_name_np = trim($dt["last_name_np"]);
		$personObj->temp_address_ward = trim($dt["temp_address_ward"]);
		$personObj->temp_address_street = trim($dt["temp_address_street"]);
		$personObj->temp_address_house = trim($dt["temp_address_house"]);

		$personId = $this->save($personObj);
		return 'person|'.$personId;
	}
	// find person by person unique id
	protected function findPersonByUid($uuid){
		//search person by names
		$wherePerson = array(
			'operator'=>'AND',
			'operand'=>array(
				0=>array(
						'operator'=>'FIELD_LIMIT',
						'field'=>'uuid',
						'style'=>'lowerequals',
						'data'=>array(
								'value'=>trim($uuid)
								)
						)
			)
		);

		$person_ids = I2CE_FormStorage::search('person', false, $wherePerson);
		if( is_array($person_ids)){
			$arr = array();
			foreach ($person_ids as $key => $value) {
				$arr[] = 'person|'.$value;
			}
			return $arr;
		}else{
			return null;
		}
		
		
	}

	public function findPerson($data){
		// var_dump(trim($data['uuid'])); exit;
		// search person by unique id
		// if exits update if not create
		if(trim($data['uuid']) != ''){
			I2CE::raiseMessage("find persion by uuid");
			$name_ids = $this->findPersonByUid($data['uuid']);
			if(count($name_ids) == 0){
				return $this->createPerson($data);
				//create person
				//return person_id;
			}
			else if(count($name_ids) == 1){
				//update existing data
				return $this->createPerson($data, current($name_ids));
				//return person_id;
			}
		}

		// if uniquer id not exits search by id type "Citizenship"
		I2CE::raiseMessage("inside find persion by Citizenship");
		if(count($data['persion_id']) > 0 ){
			I2CE::raiseMessage("find persion by Citizenship");
			$found_key = array_search('Citizenship', array_column($data['persion_id'], 'id_type'));
			if( gettype($found_key) == 'integer'){
				$pdata = $data['persion_id'][$found_key];
				//$id_number, $id_type
				$name_ids = $this->findPersonByID($pdata['id_num'], $pdata['id_type']);

				if(count($name_ids) == 0){
					return $this->createPerson($data);
				}
				else if(count($name_ids) == 1){
					return $this->createPerson($data, current($name_ids));
				}
			}
		}else{
			//ignore this data
			//create log
			I2CE::raiseMessage("Ignore record, there is no persion_id". json_encode($data));
			return null;
		}
		
		
		print_r($name_ids); exit("kishor22");
		if( !$name_ids or count($name_ids) == 0){
			// $name_ids = $this->findPersonByNames($data["surname"], $data["firstname"], $data["middle_name"], $data["citizenship"], false);
		}
	}
	// public function findPersonByReg($council, $registration_number, $registration_date){
	public function findPersonByReg($council, $registration_number, $registration_date, $createRecord = true){
		if($council && $registration_number){
			$council = $this->councilExists($council);
			$operand =  array(
						0=>array(
								'operator'=>'FIELD_LIMIT',
								'field'=>'council',
								'style'=>'equals',
								'data'=>array( 'value'=>trim($council) )
								),
						1=>array(
								'operator'=>'FIELD_LIMIT',
								'field'=>'registration_number',
								'style'=>'equals',
								'data'=>array( 'value'=>trim($registration_number) )
								)
						);
			if(strlen(registration_date) > 0){
				$operand[2] =array(
								'operator'=>'FIELD_LIMIT',
								'field'=>'registration_date',
								'style'=>'equals',
								'data'=>array( 'value'=>trim($registration_date) )
								);
			}
			//search person by identification
			$whereIdentification = array(
				'operator'=>'AND',
				'operand'=>$operand
				);

			$person_ids = I2CE_FormStorage::listFields('registration', array('parent'), false, $whereIdentification);
			if( ( count($person_ids) == 0 )  & $createRecord ){
				$dt = $this->mapped_data;
				//we need to create this person
				$personObj = $this->ff->createContainer('person');
				$personObj->firstname = trim($dt["firstname"]);
				$personObj->middle_name = trim($dt["middle_name"]);
				$personObj->surname = trim($dt["surname"]);
				$personObj->nationality = array('country', $this->countryExists($dt['nationality']) );
				$personObj->residence = array('district', trim($this->districtExists($dt['residence']['district'])));
				$personId = $this->save($personObj);
				return 'person|'.$personId;
			}
			else{
				$arr = array();
				foreach ($person_ids as $key => $value) {
					$arr[] = 'person|'.$value['parent'];
				}
				return $arr;
				// $data = current($person_ids);
				// return $data['parent'];
			}
		}
	}
	public function findPersonByID($id_number, $id_type, $createRecord = true){
		if($id_number && $id_type){
			//search person by identification
			$whereIdentification = array(
				'operator'=>'AND',
				'operand'=>array(
						0=>array(
								'operator'=>'FIELD_LIMIT',
								'field'=>'id_number',
								'style'=>'equals',
								'data'=>array(
										'value'=>trim($id_number)
										)
								),
						1=>array(
								'operator'=>'FIELD_LIMIT',
								'field'=>'id_type',
								'style'=>'equals',
								'data'=>array(
										'value'=>trim($id_type)
										)
								)
						)
				);
			$person_ids = I2CE_FormStorage::listFields('person_id', array('parent'), false, $whereIdentification);
			if( ( count($person_ids) == 0 )  ){
				return false;
			}
			else{
				$arr = array();
				foreach ($person_ids as $key => $value) {
					$arr[] = 'person|'.$value['parent'];
				}
				return $arr;
				// $data = current($person_ids);
				// return $data['parent'];
			}
		}
	}

}


/*********************************************
*
*      Execute!
*
*********************************************/

//ini_set('memory_limit','3000MB');


if (count($arg_files) != 1) {
		usage("Please specify the name of a spreadsheet to process");
}

reset($arg_files);
$file = current($arg_files);
if($file[0] == '/') {
		$file = realpath($file);
} else {
		$file = realpath($dir. '/' . $file);
}
if (!is_readable($file)) {
		usage("Please specify the name of a spreadsheet to import: " . $file . " is not readable");
}

I2CE::raiseMessage("Loading from $file");


$processor = new PersonalData_Import($file);
$processor->run();

echo "Processing Statistics:\n";
print_r( $processor->getStats());




# Local Variables:
# mode: php
# c-default-style: "bsd"
# indent-tabs-mode: nil
# c-basic-offset: 4
# End:
