#!/usr/bin/php
<?php
/*
 * © Copyright 2007, 2008 IntraHealth International, Inc.
 *
 * This File is part of iHRIS
 *
 * iHRIS is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
/**
 * The page wrangler
 *
 * This page loads the main HTML template for the home page of the site.
 * @package iHRIS
 * @subpackage DemoManage
 * @access public
 * @author Sovello Hildebrand sovellohpmgani@gmail.com
 * @copyright Copyright &copy; 2007, 2008-2013 IntraHealth International, Inc.
 * @version 4.6.0
 */
/*
php import_data.php ./data/person/person_2_4.json
php /var/lib/iHRIS/sites/nhwr-ihris/pages/import_data.php /var/lib/iHRIS/sites/nhwr-ihris/pages/data/person/person_2_4.json
php import_data.php /path/to/your/excel_sheet.csv
*/
require_once("./import_base.php");

class PersonalData_Import extends Processor{

	public function __construct($file) {
			parent::__construct($file);
	}

	//map headers from the spreadsheet
	//what you do here is change the values on the right to match what you have on the spreadsheet. comment out lines that are not in the spreadsheet
	//the values of the left are used by the script to refer to the spreadsheet columns on the right of this array.
	//the order of the columns in the spreadsheet doesn't matter

	protected function getExpectedHeaders(){
		return array(
			'surname' => 'surname',
			'firstname' => 'firstname',
			'middle_name' => 'middle_name',
			
			'nationality' => 'nationality',
			'residence' => 'residence',
			'person_parent_info' => 'person_parent_info',
			'demographic' => 'demographic',
			'person_id' => 'person_id',
			'person_contact_personal' => 'person_contact_personal',
			'person_position' => 'person_position',
			'person_contact_emergency' => 'person_contact_emergency'
			
		);
	}

	/**
	 * this function returns the id numbers 'id_number' and id type 'id_type' for that id number.
	 *
	 */

	public function getIdNumberArray(){
		//when in the spreadsheet you have multiple columns that refer to identification numbers then you have this array to handle just that
		//with the columns set above, here you come map the columns to their specific id_type.
		//so if you have say just one column with identification numbers then comment lines 80 to 87 of this file by typing /* on line 79
		//and */ on line 89
		//change the the value in id_type to be e.g. National ID to match the type of identification for this column
		return $id_numbers = array(
			0 => array(
					'id_number' => $this->mapped_data['id_num'],
					'id_type' => 'Payroll Number'
				),
			/*
			1 => array(
					'id_number' => $this->mapped_data['id_num1'],
					'id_type' => 'Salary Number'
				),
			2 => array(
				 'id_number' => $this->mapped_data['id_num'],
					'id_type' => 'Payroll Number'
			*/

		);
	}

	//this part checks to see if the nationality/country column for a record is empty it defaults to Tanzania.
	//change this to your country. otherwise it takes the value of the country in that column
	public function getNationality(){
		return empty($this->mapped_data['nationality']) ? 'Nepal' : $this->mapped_data['nationality'];
	}

	//in this part comment out if you are not adding any data for that specific item.
	//for example if there is no nextofkin data in the spreadsheet,
	//comment out line 108 by preceding it with double-slasses as in this line
	//remember to also comment out all lines in the getExpectedHeaders() function lines 46-63
	protected function _processRow(){
		$reg_data = $this->mapped_data["registration"][0];
		$personId = $this->findPerson($this->mapped_data);
		//$personId = $this->findPersonByReg($reg_data["council"], $reg_data["registration_number"], $reg_data["registration_date"]);
		I2CE::raiseMessage("personId = $personId");
		if(strlen($personId)==0)
			return;
		//start updating the child data
		
		$this->updatePersonRegistration($personId, $this->mapped_data["registration"]);
		$this->updatePersonIds($personId, $this->mapped_data['person_id']);
		$this->updateDemographic($personId, $this->mapped_data['demographic']);
		$this->updateParentInfo($personId, $this->mapped_data['person_parent_info']);
		$this->updatePersonalContact($personId, $this->mapped_data["person_contact_personal"]);
		$this->updatePersonPosition($personId, $this->mapped_data["person_position"]);
		
		/*
		Name & birthdate
Name & citizenship-no
Name & Reg-no*/


		// $this->updateEmergencyContact($personId, $this->mapped_data["person_contact_emergency"]);
		//$personId = $this->findPersonByNames($this->mapped_data['surname'], $this->mapped_data['othername'],
		//				$this->mapped_data['firstname'], $this->getNationality() );
		// $this->createPersonId($personId,$this->getIdNumberArray());
		// $this->addDemographic($personId, $this->mapped_data['birth_date'], $this->getGender($this->mapped_data['gender']),
		// 		$this->maritalStatusExists($this->mapped_data['marital_status']) );
		//$this->addNextOfKin($personId, $this->mapped_data['nxtofkin_name'],$this->mapped_data['nxtofkin_address'],$this->mapped_data['nxtofkin_relationship'],$this->mapped_data['nxtofkin_mobile']);
		//$this->addNextOfKin($personId, $this->mapped_data['nxtofkin_name'],$this->mapped_data['nxtofkin_address'],$this->mapped_data['nxtofkin_relationship'],$this->mapped_data['nxtofkin_mobile']);
		//$this->addNextOfKin($personId, $this->mapped_data['nxtofkin_name'],$this->mapped_data['nxtofkin_address'],$this->mapped_data['nxtofkin_relationship'],$this->mapped_data['nxtofkin_mobile']);
	}

	protected function updatePersonIds($personId, $Ids = array() ){
		if( ($personObj = $this->ff->createContainer($personId)) instanceof iHRIS_Person ){
			foreach( $Ids as $index=>$data){
				$formObj = $this->ff->createContainer('person_id');
				$formObj->getField('id_num')->setValue(trim($data['id_num']));
				$formObj->getField('id_type')->setValue(array('id_type', $this->idTypeExists($data['id_type']) ) );
				$formObj->getField('country')->setValue(array('country', $this->countryExists($data['country']) ) );
				$formObj->getField('place')->setValue(trim($data['place']['district']));
				// $formObj->getField('place')->setValue(array('district', $this->districtExists($data['place']['district']) ) );
				$formObj->setParent($personId);
				$this->save($formObj);
			}
		}
	}
	protected function updatePersonPosition($personId, $position = array() ){
		if( ($personObj = $this->ff->createContainer($personId)) instanceof iHRIS_Person ){
			//set person's position
			$positionId = $this->getPositionId($position);
			$pos = 'position|'.$positionId;
			I2CE::raiseMessage("Position ID = ".$pos);
			//now udpate the status of used position.
			$formObjP = $this->ff->createContainer($pos);
			$formObjP->setId($pos);
			I2CE::raiseMessage("Get Position ID = ".$formObjP->getId());
			$formObjP->statusOnly();
			$formObjP->getField('status')->setValue(array('position_status','closed'));
			$this->save($formObjP);

			$formObjPP = $this->ff->createContainer('person_position');
			$formObjPP->getField('start_date')->setValue(I2CE_Date::fromDB(trim($position['start_date'])));
			$formObjPP->setParent($personId);
			$formObjPP->getField('position')->setValue(array('position', $positionId));
			$this->save($formObjPP);
			

			//save the salary
			$formObjS = $this->ff->createContainer('salary');
			$salary =  array("currency|".$this->currencyExists($position['currency']) , $position["salary"]);
			$formObjS->getField('salary')->setValue($salary);
			$formObjS->getField('start_date')->setValue(I2CE_Date::fromDB(trim($position['start_date'])));
			$formObjS->setParent($personId);
			$this->save($formObjS);
		}
	}

	public function getPositionId($data, $status = "open"){
		$jobId = $this->jobExists($data['job']);
		$facilityId = $this->facilityExists($data['facility']);
		//search position by title
		$wherePosition = array(
			'operator'=>'AND',
			'operand'=>array(
				0=>array(
						'operator'=>'FIELD_LIMIT',
						'field'=>'title',
						'style'=>'lowerequals',
						'data'=>array(
								'value'=>trim($data['title'])
								)
						),
				1=>array(
						'operator'=>'FIELD_LIMIT',
						'field'=>'job',
						'style'=>'lowerequals',
						'data'=>array(
								'value'=>trim("job|".$jobId)
								)
						),
				2=>array(
						'operator'=>'FIELD_LIMIT',
						'field'=>'facility',
						'style'=>'lowerequals',
						'data'=>array(
								'value'=>trim("facility|".$facilityId)
								)
						),
				3=>array(
						'operator'=>'FIELD_LIMIT',
						'field'=>'status',
						'style'=>'lowerequals',
						'data'=>array(
								'value'=>trim("position_status|".$status)
								)
						)
				)
			);
		$position = I2CE_FormStorage::search('position', false, $wherePosition);
		I2CE::raiseMessage("Position COUNT = ".count($position));
		if( count($position) > 1 ){
			return current($position);
		}
		else if(count($position) == 1){
			return $position[0];
		}
		elseif( (count($position) == 0) ){
			//we need to create this position
			$formObjPos = $this->ff->createContainer('position');
			$formObjPos->getField('job')->setValue(array('job', $jobId));
			$code = trim($data['code']);
			if(0 == strlen($code))
				$code = trim($data['title']);
			$formObjPos->getField('code')->setValue($code);
			$formObjPos->getField('title')->setValue(trim($data['title']));
			$formObjPos->getField('facility')->setValue(array('facility', $facilityId));
			$formObjPos->getField('status')->setValue(array('position_status','open'));
			$positionId = $this->save($formObjPos);
			return $positionId;
		}
	}

	public function positionExists($data){
		//creating position

		$formObj = $this->ff->createContainer('position');
		$formObj->getField('job')->setValue(array('job', $this->jobExists($data['job']) ) );
		$formObj->getField('title')->setValue(trim($data['title']));
		$formObj->getField('facility')->setValue(array('facility', $this->facilityExists($data['facility']) ) );
		return $this->save($formObj);
		//return $this->checkTitleExists('iHRIS_PersonPosition', $title);
	}
	protected function updatePersonRegistration($personId, $registration = array() ){
		if( ($personObj = $this->ff->createContainer($personId)) instanceof iHRIS_Person ){
			foreach( $registration as $index=>$data){
				$formObj = $this->ff->createContainer('registration');
				$formObj->getField('registration_number')->setValue(trim($data['registration_number']));
				if(isset($data['registration_date']) && strlen(trim($data['registration_date']))>0){
					$formObj->getField('registration_date')->setValue(I2CE_Date::fromDB(trim($data['registration_date'])));
				}
				$formObj->getField('council')->setValue(array('council', $this->councilExists($data['council']) ) );
				$formObj->getField('registration_date')->getValue();
				// $formObj->getField('country')->setValue(array('country', $this->countryExists($data['country']) ) );
				// $formObj->getField('place')->setValue(array('district', $this->districtExists($data['place']['district']) ) );
				$formObj->setParent($personId);
				$this->save($formObj);
			}
		}
	}

	protected function updateParentInfo($personId, $parentDetail){
		if($personId)
		$where = array(
			'operator' => 'FIELD_LIMIT',
			'field' => 'parent',
			'style' => 'lowerequals',
			'data' => array(
					'value' => $personId
				)
			);

		$parentId = I2CE_FormStorage::search('person_parent_info', false, $where );
		if( count($parentId) == 0){
			$formObj = $this->ff->createContainer('person_parent_info');
		}
		else
			$formObj = $this->ff->createContainer('person_parent_info|'.$parentId);
		$formObj->getField('father_surname')->setValue(trim($parentDetail['father_surname']));
		$formObj->getField('father_firstname')->setValue(trim($parentDetail['father_firstname']));
		$formObj->getField('father_middlename')->setValue(trim($parentDetail['father_middlename']));
		$formObj->getField('mother_surname')->setValue(trim($parentDetail['mother_surname']));
		$formObj->getField('mother_firstname')->setValue(trim($parentDetail['mother_firstname']));
		$formObj->getField('mother_middlename')->setValue(trim($parentDetail['mother_middlename']));
		$formObj->getField('husband_surname')->setValue(trim($parentDetail['husband_surname']));
		$formObj->getField('husband_firstname')->setValue(trim($parentDetail['husband_firstname']));
		$formObj->getField('husband_middlename')->setValue(trim($parentDetail['husband_middlename']));

		$formObj->setParent($personId);
		$this->save($formObj);
	}

	protected function updatePersonalContact($personId, $contact){
		if(strlen($personId)==0)
			return;
		if(trim($contact['telephone']) == 0 && trim($contact['mobile_phone']) && trim($contact['email']))
			return;
		$where = array(
			'operator' => 'FIELD_LIMIT',
			'field' => 'parent',
			'style' => 'lowerequals',
			'data' => array(
					'value' => $personId
				)
			);

		$parentId = I2CE_FormStorage::search('person_contact_personal', false, $where );
		if( count($parentId) == 0){
			$formObj = $this->ff->createContainer('person_contact_personal');
		}
		else
			$formObj = $this->ff->createContainer('person_contact_personal|'.$parentId);
		$formObj->getField('telephone')->setValue(trim($contact['telephone']));
		$formObj->getField('mobile_phone')->setValue(trim($contact['mobile_phone']));
		$formObj->getField('alt_telephone')->setValue(trim($contact['alt_telephone']));
		$formObj->getField('email')->setValue(trim($contact['email']));
		$formObj->setParent($personId);
		$this->save($formObj);
	}

	protected function updateEmergencyContact($personId, $contact){
		if($personId)
		$where = array(
			'operator' => 'FIELD_LIMIT',
			'field' => 'parent',
			'style' => 'lowerequals',
			'data' => array(
					'value' => $personId
				)
			);

		$parentId = I2CE_FormStorage::search('person_contact_emergency', false, $where );
		if( count($parentId) == 0){
			$formObj = $this->ff->createContainer('person_contact_emergency');
		}
		else
			$formObj = $this->ff->createContainer('person_contact_emergency|'.$parentId);
		$formObj->getField('surname')->setValue(trim($contact['surname']));
		$formObj->getField('middlename')->setValue(trim($contact['middlename']));
		$formObj->getField('firstname')->setValue(trim($contact['firstname']));
		$formObj->getField('telephone')->setValue(trim($contact['telephone']));
		$formObj->getField('mobile_phone')->setValue(trim($contact['mobile_phone']));
		$formObj->getField('alt_telephone')->setValue(trim($contact['alt_telephone']));
		$formObj->getField('email')->setValue(trim($contact['email']));
		$formObj->setParent($personId);
		$this->save($formObj);
	}
	/****************************************************************************
	 *                                                                          *
	 *   DON'T EDIT BEYOND THIS POINT UNLESS YOU KNOW WHAT YOU WANT TO ACHIEVE  *
	 *                                                                          *
	 ****************************************************************************/
	protected function createPersonId($personId, $id_nums = array() ){
		if( ($personObj = $this->ff->createContainer($personId)) instanceof iHRIS_Person ){
			foreach( $id_nums as $index=>$data){
				$formObj = $this->ff->createContainer('person_id');
				$formObj->getField('id_num')->setValue(trim($data['id_number']));
				$formObj->getField('id_type')->setValue(array('id_type', $this->idTypeExists($data['id_type']) ) );
				$formObj->setParent($personId);
				$this->save($formObj);
			}
		}
	}

	protected function updateDemographic($personId, $demo){
		if(strlen($personId)==0)
			return;
		$where = array(
			'operator' => 'FIELD_LIMIT',
			'field' => 'parent',
			'style' => 'lowerequals',
			'data' => array(
					'value' => $personId
				)
			);

		$demoId = I2CE_FormStorage::search('demographic', false, $where );
		I2CE::raiseMessage("personId = $personId, demoId count ".count($demoId));
		if( count($demoId) == 0){
			$formObj = $this->ff->createContainer('demographic');
		}
		else
			$formObj = $this->ff->createContainer("demographic|".$demoId);

		$formObj->getField('birth_date')->setValue(I2CE_Date::fromDB(trim($demo['birth_date'])));
		$formObj->getField('gender')->setValue( array('gender', $this->getGender($demo['gender'])));
		// $formObj->getField('marital_status')->setValue( array('marital_status', $this->maritalStatusExists($demo['marital_status'])) );
		$co = $this->countryExists($demo['citizenship_at_birth']);
		$formObj->getField('nationality_at_birth')->setValue( array('country', $co));
		$formObj->getField('birth_location')->setValue(array('district', $this->districtExists($demo['brith_location']['district'])));
		$formObj->setParent($personId);
		$this->save($formObj);
	}
	// public function checkNameExists($formName, $name){
	// 	return $this->checkExists($formName, 'name', $name);
	// }
	// public function checkTitleExists($formName, $title){
	// 	return $this->checkExists($formName, 'title', $title);
	// }
	// public function checkCodeExists($formName, $code){
	// 	return $this->checkExists($formName, 'code', $code);
	// }
	// public function checkExists($formName, $field, $value){
	// 	$value_t = strtolower(trim($value));
	// 	$where = array(
	// 		'operator'=>'FIELD_LIMIT',
	// 		'field'=>$field,
	// 		'style'=>'lowerequals',
	// 		'data'=>array(
	// 				'value' => $value_t
	// 			)
	// 	);
	// 	if(strlen($value_t) > 0)
	// 		$items = I2CE_FormStorage::search($formName, false, $where);
	// 	else{
	// 		return null;
	// 	}
	// 	if(count($items) >= 1 ){
	// 		return current($items); //like 123432
	// 	}
	// 	elseif(count($items) == 0){
	// 		$formObj = $this->ff->createContainer($formName);
	// 		$formObj->getField($field)->setValue($value_t);
	// 		$id = $this->save($formObj);
	// 		return $id; //like 123432
	// 	}
	// }
	public function countryExists($name){
		return $this->checkNameExists('country', $name);
	}
	public function currencyExists($name){
		return $this->checkNameExists('currency', $name);
	}
	
	public function idTypeExists($name){
		return $this->checkNameExists('id_type', $name);
	}
	public function jobExists($title){
		return $this->checkTitleExists('job', $title);
	}
	public function facilityExists($name){
		return $this->checkNameExists('facility', $name);
	}
	public function councilExists($name){
		return $this->checkNameExists('council', $name);
	}
	public function districtExists($name){
		return $this->checkNameExists('district', $name);
	}

	public function maritalStatusExists($name){
		return $this->checkNameExists('marital_status', $name);
	}

	public function getGender( $gender ){

		$g = strtolower(trim($gender));
		if( ($g == 'f') || ($g == 'female') || (substr($gender,0,1) == 'f') ){
			return 'F';
		}
		if( ($g == 'm') || ($g == 'male') || (substr($gender,0,1) == 'm') ){
			return 'M';
		}
	}

	public function findPersonByNames($surname, $firstname, $middle_name, $country, $createRecord = true){
		//search person by names
		$wherePerson = array(
			'operator'=>'AND',
			'operand'=>array(
				0=>array(
						'operator'=>'FIELD_LIMIT',
						'field'=>'surname',
						'style'=>'lowerequals',
						'data'=>array(
								'value'=>trim($surname)
								)
						),
				1=>array(
						'operator'=>'FIELD_LIMIT',
						'field'=>'firstname',
						'style'=>'lowerequals',
						'data'=>array(
								'value'=>trim($firstname)
								)
						),
				2=>array(
						'operator'=>'FIELD_LIMIT',
						'field'=>'middle_name',
						'style'=>'lowerequals',
						'data'=>array(
								'value'=>trim($middle_name)
								)
						),
				3=>array(
						'operator'=>'FIELD_LIMIT',
						'field'=>'country',
						'style'=>'lowerequals',
						'data'=>array(
								'value'=>array('country', $this->countryExists($country) )
								)
						),
				)
			);

		$person_ids = I2CE_FormStorage::search('person', false, $wherePerson);
		if( count($person) > 1 || $createRecord == false){
			//$this->addBadRecord("This person already exists in duplicates");
			// return false;
			$arr = array();
			foreach ($person_ids as $key => $value) {
				$arr[] = 'person|'.$value;
			}
			return $arr;
			// return $person_ids;
		}
		elseif(count($person_ids) == 1){
			return 'person|'.$person_ids[0];
		}
		elseif( (count($person_ids) == 0) && createRecord ){
			//we need to create this person
			$personObj = $this->ff->createContainer('person');
			$personObj->firstname = trim($firstname);
			$personObj->othername = trim($othername);
			$personObj->surname = trim($surname);
			$personObj->nationality = array('country', $this->countryExists($country) );
			$personId = $this->save($personObj);
			return 'person|'.$personId;
		}
	}

	protected function createPerson($data){
		$dt = $data;
		$personObj = $this->ff->createContainer('person');
		$personObj->firstname = trim($dt["firstname"]);
		$personObj->middle_name = trim($dt["middle_name"]);
		$personObj->surname = trim($dt["surname"]);
		$personObj->nationality = array('country', $this->countryExists($dt['nationality']) );
		$personObj->residence = array('district', trim($this->districtExists($dt['residence']['district'])
	));
		$personId = $this->save($personObj);
		return 'person|'.$personId;
	}

	public function findPerson($data){
		$name_ids = $this->findPersonByNames($data["surname"], $data["firstname"], $data["middle_name"], $data["nationality"], false);
		if(count($name_ids) == 0){
			return $this->createPerson($data);
			//create person
			//return person_id;
		}
		else if(count($name_ids) == 1){
			return current($name_ids);
			//return person_id;
		}
		else{
			//use the provided ids to do granular search.
		}
		//citizenship
		$citizenship = array();
		$citzen_ids = array();
		if(count($data["person_id"]) > 0){
			foreach ($data["person_id"] as $key => $value) {
				if($value[id_type] == "Citizenship"){
					$citizenship = $value;
					break;
				}
			}
			if(count($citizenship)){
				$citzen_ids = $this->findPersonByNames($citizenship["id_num"], $citizenship["id_type"],false);
				if(count($citzen_ids) == 0){
					if(count($name_ids) == 0){
						return $this->createPerson($data);
					}
					//create person
					//return person_id;
				}
				else if(count($citzen_ids) == 1){
					return current($citzen_ids);
					//return person_id;
				}
				else{
					//use the provided ids to do granular search.
				}
			}
		}
		//registration
		$reg = array();
		$reg_ids = array(); 
		if(count($data["registration"]) > 0){
			$reg = $data["registration"];
			$reg_ids = $this->findPersonByNames($regs["council"], $reg["registration_number"], $reg["registration_date"],false);
			if(count($reg_ids) == 0){
				if(count($name_ids) == 0){
					return $this->createPerson($data);
				}
				//create person
				//return person_id;
			}
			else if(count($reg_ids) == 1){
				return current($reg_ids);
				//return person_id;
			}
			else{
				//use the provided ids to do granular search.
			}
		}
		

		//insersection
		if(is_array($name_ids)==false)
			$name_ids = array();
		if(is_array($citzen_ids)==false)
			$citzen_ids = array();
		if(is_array($reg_ids)==false)
			$reg_ids = array();
		$intersect_ct = array();
		if(count($name_ids)> 0 && count($citzen_ids) > 0){
			$intersect_ct = array_intersect($name_ids, $citzen_ids);
		}
		$intersect_r = array();
		if(count($name_ids)> 0 && count($reg_ids) > 0){
			$intersect_r = array_intersect($name_ids, $reg_ids);
		}
		$intersect = array_intersect($intersect_ct, $intersect_r);
		if(count($intersect) > 0)
			return current($intersect);

		if(count($intersect_ct) > 0)
			return current($intersect_ct);
		if(count($intersect_r) > 0)
			return current($intersect_r);
		return current($name_ids);
	}
	// public function findPersonByReg($council, $registration_number, $registration_date){
	public function findPersonByReg($council, $registration_number, $registration_date, $createRecord = true){
		if($council && $registration_number){
			$council = $this->councilExists($council);
			$operand =  array(
						0=>array(
								'operator'=>'FIELD_LIMIT',
								'field'=>'council',
								'style'=>'equals',
								'data'=>array(
										'value'=>trim($council)
										)
								),
						1=>array(
								'operator'=>'FIELD_LIMIT',
								'field'=>'registration_number',
								'style'=>'equals',
								'data'=>array(
										'value'=>trim($registration_number)
										)
								)
						);
			if(strlen(registration_date) > 0){
				$operand[2] =array(
								'operator'=>'FIELD_LIMIT',
								'field'=>'registration_date',
								'style'=>'equals',
								'data'=>array(
										'value'=>trim($registration_date)
									)
								);
			}
			//search person by identification
			$whereIdentification = array(
				'operator'=>'AND',
				'operand'=>$operand
				);

			$person_ids = I2CE_FormStorage::listFields('registration', array('parent'), false, $whereIdentification);
			if( ( count($person_ids) == 0 )  & $createRecord ){
				$dt = $this->mapped_data;
				//we need to create this person
				$personObj = $this->ff->createContainer('person');
				$personObj->firstname = trim($dt["firstname"]);
				$personObj->middle_name = trim($dt["middle_name"]);
				$personObj->surname = trim($dt["surname"]);
				$personObj->nationality = array('country', $this->countryExists($dt['nationality']) );
				$personObj->residence = array('district', trim($this->districtExists($dt['residence']['district'])));
				$personId = $this->save($personObj);
				return 'person|'.$personId;
			}
			else{
				$arr = array();
				foreach ($person_ids as $key => $value) {
					$arr[] = 'person|'.$value['parent'];
				}
				return $arr;
				// $data = current($person_ids);
				// return $data['parent'];
			}
		}
	}
	public function findPersonByID($id_number, $id_type, $createRecord = true){
		if($id_number && $id_type){
			//search person by identification
			$whereIdentification = array(
				'operator'=>'AND',
				'operand'=>array(
						0=>array(
								'operator'=>'FIELD_LIMIT',
								'field'=>'id_number',
								'style'=>'equals',
								'data'=>array(
										'value'=>trim($id_number)
										)
								),
						1=>array(
								'operator'=>'FIELD_LIMIT',
								'field'=>'id_type',
								'style'=>'equals',
								'data'=>array(
										'value'=>trim($id_type)
										)
								)
						)
				);
			$person_ids = I2CE_FormStorage::listFields('person_id', array('parent'), false, $whereIdentification);
			if( ( count($person_ids) == 0 )  ){
				return false;
			}
			else{
				$arr = array();
				foreach ($person_ids as $key => $value) {
					$arr[] = 'person|'.$value['parent'];
				}
				return $arr;
				// $data = current($person_ids);
				// return $data['parent'];
			}
		}
	}


	protected function addNextOfKin($personId, $name, $address, $relationship, $mobile ){
		if( $personId )
			$where = array(
				'operator' => 'FIELD_LIMIT',
				'field' => 'parent',
				'style' => 'lowerequals',
				'data' => array(
						'value' => $personId
					)
				);

		$nxtofkin = I2CE_FormStorage::search('nextofkin', false, $where );
		if( count($nxtofkin) == 0){
			$formObj = $this->ff->createContainer('nextofkin');
			$formObj->getField('name')->setValue( $this->mapped_data['nxtofkin_name'] );
			$formObj->getField('relationship')->setValue( $this->mapped_data['nxtofkin_relationship'] );
			$formObj->getField('telephone')->setValue( $this->mapped_data['nxtofkin_telephone'] );
			$formObj->getField('address')->setValue( $this->mapped_data['nxtofkin_address'] );
			$formObj->getField('mobile_phone')->setValue( $this->mapped_data['nxtofkin_mobile_phone'] );
			$formObj->getField('email')->setValue( $this->mapped_data['nxtofkin_email'] );
			$formObj->setParent($personId);
			$this->save($formObj);
		}
	}

}


/*********************************************
*
*      Execute!
*
*********************************************/

//ini_set('memory_limit','3000MB');


if (count($arg_files) != 1) {
		usage("Please specify the name of a spreadsheet to process");
}

reset($arg_files);
$file = current($arg_files);
if($file[0] == '/') {
		$file = realpath($file);
} else {
		$file = realpath($dir. '/' . $file);
}
if (!is_readable($file)) {
		usage("Please specify the name of a spreadsheet to import: " . $file . " is not readable");
}

I2CE::raiseMessage("Loading from $file");


$processor = new PersonalData_Import($file);
$processor->run();

echo "Processing Statistics:\n";
print_r( $processor->getStats());




# Local Variables:
# mode: php
# c-default-style: "bsd"
# indent-tabs-mode: nil
# c-basic-offset: 4
# End:
